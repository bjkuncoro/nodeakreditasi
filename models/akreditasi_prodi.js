'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class akreditasi_prodi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  akreditasi_prodi.init({
    prodi: DataTypes.STRING,
    strata: DataTypes.STRING,
    wilayah: DataTypes.STRING,
    noSk: DataTypes.STRING,
    tahunSk: DataTypes.STRING,
    peringkat: DataTypes.STRING,
    kadaluarsa: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'akreditasi_prodi',
  });
  return akreditasi_prodi;
};