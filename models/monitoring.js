'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class monitoring extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  monitoring.init({
    jenis: DataTypes.STRING,
    nilai: DataTypes.INTEGER,
    kipas1: DataTypes.STRING,
    kipas2: DataTypes.STRING,
    lampu1: DataTypes.STRING,
    lampu2: DataTypes.STRING,

  }, {
    sequelize,
    modelName: 'monitoring',
  });
  return monitoring;
};