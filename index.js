'use strict'

var express     =   require('express');
var app         =   express();
const axios     =   require('axios');
const model     =   require('./models/index');
// const getData   =   ()=>{
//     axios.get('https://www.banpt.or.id/direktori/model/dir_prodi/get_hasil_pencariannew.php').then(res=>{
//         // console.log(res.data)
//         const   d   =   res.data.data.filter(i=>{
//             return i[0] ==  'Universitas Tanjungpura'
//         })
//         // console.log(d)
//         f = d
//     }).catch(err=>{
//         console.log({err})
//     })
// }

// getData()
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
  });
  
model.sequelize
.authenticate()
.then(() => {
    console.log('Connection has been established successfully.');
})
.catch(err => {
    console.error('Unable to connect to the database:', err);
});

app.get('/getData/2019',(req,res)=>{
    model.akreditasi_prodi.findAll().then(data=>{
        console.log(data.length)
        res.json(data)
    }).catch(err=>{
        console.log(err)
    })
})

app.get('/kirimData/:data1/:data2/:lampu1/:lampu2',(req,res)=>{
    console.log('arus :'+req.params.data1, 'lux :'+req.params.data2,req.params.lampu1,req.params.lampu2)
    res.json({enak:req.params.data1})

    model.monitoring.create({
        jenis:'arus',
        nilai:req.params.data1,
        lampu1:req.params.lampu1,
        lampu2:req.params.lampu2,
    }).then(data=>{
        // console.log(data)
        // res.json(data)
    }).catch(err=>{
        console.log(err)
    })

    model.monitoring.create({
        jenis:'lux',
        nilai:req.params.data2,
        lampu1:req.params.lampu1,
        lampu2:req.params.lampu2,
    }).then(data=>{
        // res.json(data)
    }).catch(err=>{
        console.log(err)
    })
})

app.get('/kirimData2/:data1/:data2/:data3/:kipas1/:kipas2',(req,res)=>{
    console.log(req.params.data1,req.params.data2,req.params.data3,req.params.kipas1,req.params.kipas2)
    model.monitoring.create({
        jenis:'suhu',
        nilai:req.params.data1,
        kipas1:req.params.kipas1,
        kipas2:req.params.kipas2,
    }).then(data=>{
        // res.json(data)
    }).catch(err=>{
        console.log(err)
    })

    model.monitoring.create({
        jenis:'kelembaban',
        nilai:req.params.data2,
        kipas1:req.params.kipas1,
        kipas2:req.params.kipas2,
    }).then(data=>{
        // res.json(data)
    }).catch(err=>{
        console.log(err)
    })

    model.monitoring.create({
        jenis:'tegangan',
        nilai:req.params.data3,
        kipas1:req.params.kipas1,
        kipas2:req.params.kipas2,
    }).then(data=>{
        // res.json(data)
    }).catch(err=>{
        console.log(err)
    })

})

app.get('/getDataSensor',(req,res)=>{
    model.monitoring.findAll({
        limit:100,
        order: [ [ 'createdAt', 'DESC' ]]
    }).then(data=>{
        console.log(data.length)
        res.json(data)
    }).catch(err=>{
        console.log(err)
    })
})

app.get('/getData',(req,res)=>{
    // res.json({key:'taik'})
console.log('masuk req')    
axios.get('https://www.banpt.or.id/direktori/model/dir_prodi/get_hasil_pencariannew.php').then(resp=>{
//        console.log(res.data)
        const   d   =   resp.data.data.filter(i=>{
            return i[0] ==  'Universitas Tanjungpura' || i[0] ==  'Universitas Tanjungpura, Pontianak'
        })
        console.log(d.length)
        const f =   []
        d.map(i=>{
            f.push({perguruanTinggi:i[0],prodi:i[1],strata:i[2],wilayah:i[3],noSK:i[4],tahunSK:i[5],peringkat:i[6],kadaluarsa:i[7],status:i[8]})
        })
        res.json(f)
    }).catch(err=>{
        console.log({err})
    })
})
app.listen(2000,()=>{
    console.log('Running . . . .',new Date())
})
