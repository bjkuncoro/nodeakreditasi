'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return Promise.all([
      queryInterface.addColumn('monitorings', 'kipas1', {
        type: Sequelize.STRING,
        allowNull: true,
      }),
      queryInterface.addColumn('monitorings', 'kipas2', {
        type: Sequelize.STRING,
        allowNull: true,
      }),
      queryInterface.addColumn('monitorings', 'lampu1', {
        type: Sequelize.STRING,
        allowNull: true,
      }),
      queryInterface.addColumn('monitorings', 'lampu2', {
        type: Sequelize.STRING,
        allowNull: true,
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return Promise.all([queryInterface.removeColumn(',monitorings', 'kipas1'),
    queryInterface.removeColumn(',monitorings', 'kipas2'),
    queryInterface.removeColumn(',monitorings', 'lampu1'),
    queryInterface.removeColumn(',monitorings', 'lampu2')]);
  }
};
