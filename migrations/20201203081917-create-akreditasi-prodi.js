'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('akreditasi_prodis', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      prodi: {
        type: Sequelize.STRING
      },
      strata: {
        type: Sequelize.STRING
      },
      wilayah: {
        type: Sequelize.STRING
      },
      noSk: {
        type: Sequelize.STRING
      },
      tahunSk: {
        type: Sequelize.STRING
      },
      peringkat: {
        type: Sequelize.STRING
      },
      kadaluarsa: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('akreditasi_prodis');
  }
};